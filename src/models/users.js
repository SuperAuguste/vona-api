const Model = require("../../lib/model");

class Users extends Model {

	constructor () {

		super("Users");

	}

	fields () {

		return {

			id: {
				type: "number",
				required: true

			},
			email: {
				type: "string",
				required: false
			},
			username: {
				type: "string",
				required: false
			},
			password: {
				type: "string",
				required: false
			}

		}

	}

}

module.exports = Users;
