/**
 * The Database
 */
const localDb = require("./localDb");

/**
 * The Models
 */
const Users = require("./models/users");

var db = new localDb();

var m = new Users();
db.model(m);

// ** DISABLE THE FOLLOWING LINE DURING PROD
m.insert({

	id: 0,
	email: "admin@black.hole",
	username: "admin",
	password: "admin"

});

module.exports = db;
