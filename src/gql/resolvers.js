const db = require("../db");
const sessions = require("../sessions");

module.exports = {

	Query: {

		async loginByEmail (_, { email, password }) {

			var user = await db.model("Users").findOne({

				email,
				password

			});

			if (!user) return "";

			return sessions.session(
				{id: user.id});

		},

		async loginByUsername (_, { username, password }) {

			var user = await db.model("Users").findOne({

				username,
				password

			});

			if (!user) return "";

			return sessions.session(
				{id: user.id});
			
		}

	}	
	
}
