const fs = require("fs");
const path = require("path");
const chalk = require("chalk").default;

const LOG = console.log;
const INFO = console.info;

const LOGGERS = [];

class Logger {

	constructor (name, file) {

		this.name = name;
		this.messages = [];

	}

	globalize () {

		((f) => {

			console.log = (data) => {
		
				getLogger(this.name).log(data);
		
			}
		
		})(console.log);

		((f) => {

			console.info = (data) => {
		
				getLogger(this.name).info(data);
		
			}
		
		})(console.info);

	}

	save () {

		return new Promise((resolve, reject) => {



		});

	}

	output (message) {

		LOG(message);
		this.messages.push(message);

	}

	log (message) {

		this.output(chalk.gray(`[${this.name} @ ${Date.now()}]`) + ` ${message}`);

	}

	info (message) {

		this.output(chalk.blue(`[${this.name} @ ${Date.now()}]`) + ` ${message}`);

	}

	success (message) {

		this.output(chalk.green(`[${this.name} @ ${Date.now()}]`) + ` ${message}`);

	}

}

global.getLogger = module.exports = name => {

	var f = LOGGERS.find(l => l.name === name);

	if (!f) return LOGGERS[LOGGERS.push(new Logger(name)) - 1];
	else return f;

}
