const jwt = require("jsonwebtoken");

var key = "KeyThatWillEventuallyBeChanged(Hopefully)";

module.exports = {

	session (user, token) {

		if (token) {

			try {
				
				var j = jwt.verify(token, key);

				if (j.id !== user.id) return;

				return j;

			} catch {

				return;

			}

		} else {

			return jwt.sign(user, key);

		}

	}

}
