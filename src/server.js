require("./logger");

const db = require("./db");
const fs = require("fs");
const ini = require("ini");
const path = require("path");
const express = require("express");

const { ApolloServer, gql } = require("apollo-server-express");
const typeDefs = gql`${fs.readFileSync(path.join(__dirname, "gql/defs.gql")).toString()}`;
const cors = require("cors");

const config = ini.parse(fs.readFileSync(path.join(__dirname, "..", "config", "config.ini")).toString());

var app = express();

app.use(cors());

const server = new ApolloServer({ typeDefs, resolvers: require("./gql/resolvers") });

server.applyMiddleware({

	app

});

app.listen(config.port || 3000, () => {
	
	getLogger("server").log(`Server is listening on port ${config.port || 3000}`);

});

