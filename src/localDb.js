const Database = require("../lib/database");

class LocalDb extends Database {

	constructor () {

		super();
		this.data = [];

	}

	insert (data) {

		this.data.push(data);

	}

	find (query) {

		return this.data.filter(e => {

			for (const key in query) {

				if (query.hasOwnProperty(key)) {
				
					const element = query[key];
				
					if (e[key] !== element) return false;
					
				}

			}

			return true;

		});

	}

	update (q_old, q_new) {

		var ff = this.find(q_old);

		for (const f of ff) {
			
			var q = {

				...f,
				...q_new
	
			}

			this.data[this.data.indexOf(f)] = q;

		}

	}

}

module.exports = LocalDb;
