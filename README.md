# Vona API

This repository contains the source code of Vonadrive's API.

## Usage

To start using the API, follow these simple steps:

1. Run `npm install` or `yarn` (yarn is better)
2. Run `npm run start`
3. Go to the port specified in the configuration file (default is 3000)
4. Start using the REST API located at `/api` or the GraphQL Playground located at `/graphql`

## Progress

Sector | Progress
--|--
`Models` | **10%**
`GraphQL` | **100%**
`CQL` | **0%**
`REST API` | **0%**
