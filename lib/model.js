const One = require("./one");
const Database = require("./database");
const pluralize = require("pluralize");

class Model {

	constructor (name) {

		/**
		 * @type {Database}
		 */
		this.db = null;

		this.name = name;

	}

	async one (query) {

		var one = new One(pluralize.singular(this.name), this, query);
		await one.load();
		return one;

	}

	fields () {}

	_validate (data, extra = {}) {

		var labels = this.fields();

		for (const key in labels) {

			if (labels.hasOwnProperty(key)) {

				const labelData = labels[key];
				
				if (extra.type !== "update" && labelData.required && data[key] === undefined) {

					return {

						isValid: false,
						message: `Missing required field "${key}"`

					}
					
				}

			}

		}

		return {

			isValid: true

		}

	}

	validate (data, extra) {

		var v = this._validate(data, extra);

		if (!v.isValid) {

			throw new Error(`Invalid data provided in model "${this.name}": ${v.message}`);

		}

	}

	async insert (data) {

		this.validate(data);

		await this.db.insert({

			model: this.name,

			...data

		});

	}

	async find (query) {

		return await this.db.find({

			model: this.name,

			...query

		});

	}

	async findOne (query) {

		return (await this.find(query))[0];

	}

	async update (q_old, q_new) {

		this.validate(q_new, {

			type: "update"

		});

		return await this.db.update({

			model: this.name

		}, q_new);

	}

}

module.exports = Model;
