const Model = require("./model");

class One {

	/**
	 * Create a new `One` instance
	 * 
	 * @param {String} name 
	 * @param {Model} model 
	 * @param {object} query 
	 */
	constructor (name, model, query) {

		this._data = {};
		this.data = {};

		this.name = name;
		this.model = model;
		this.query = query;

	}

	async load () {

		var self = this;
		this._data = {...(await this.model.find(this.query))[0]};

		this.data = new Proxy(this._data, {

			async set (target, key, value) {
			
				var b = {...target};

				target[key] = value;

				await self.model.update(b, target);

			},

			get (target, key) {

				return target[key];

			}

		});

	}

}

module.exports = One;
