const Model = require("../lib/model");

/**
 * @class
 * 
 * Database skeleton class
 */
class Database {

	constructor () {

		/**
		 * @typeof Model[]
		 */
		this.models = [];

	}

	/**
	 * Add a model to the database
	 * 
	 * @param {Model|String} model The model to be added
	 * 
	 * @returns {Model}
	 */
	model (model) {

		if (typeof model === "string") {

			return this.models.find(data => data.name === model);

		} else {

			model.db = this;
			var mi = this.models.push(model) - 1;

			return this.models[mi];

		}

	}

	insert () {

		

	}

	find () {
		


	}

	update () {



	}

}

module.exports = Database;
